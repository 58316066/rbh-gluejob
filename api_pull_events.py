import sys
import requests
import json
import csv
import datetime
import boto3
from botocore.exceptions import NoCredentialsError

from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame

from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, col, explode, lit
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, ArrayType
from awsglue.dynamicframe import DynamicFrameCollection
from pyspark.sql import Row
# from default_api_pull_event import config



# Setup Params
# GET_AGRS = config.get('GET_AGRS')

# print("start process")
args = getResolvedOptions(sys.argv, ["JOB_NAME", "EVENT_NAME" , "APP_ID", "EVENT_FROM_DT", "EVENT_TO_DT", "MAXIMUM_ROW","FIELDS","API_TOKEN","TIMEZONE","FIELD_PARTITION_S3","S3_OUTPUT_FILE", "BUCKET_NAME", "PATH_NAME"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Variables declaration
# rec_start_date = (datetime.datetime.now() + datetime.timedelta(hours = 7)).strftime('%Y-%m-%d %H:%M:%S')
rec_date=datetime.datetime.now().strftime('%Y%m%d')
report_type = args["EVENT_NAME"]
app_id=args["APP_ID"]
start_evn_dt=args["EVENT_FROM_DT"]
end_evn_dt=args["EVENT_TO_DT"]
maximum_rows=args["MAXIMUM_ROW"]
fields=args["FIELDS"]
api_token=args["API_TOKEN"]
timezone=args["TIMEZONE"]
FIELD_PARTITION_S3=args["FIELD_PARTITION_S3"]
S3_OUTPUT_FILE = args["S3_OUTPUT_FILE"]
BUCKET_NAME =  args["BUCKET_NAME"]
PATH_NAME =  args["PATH_NAME"]
csv_url = f'https://hq.appsflyer.com/export/{app_id}/{report_type}/v5?api_token={api_token}&from={start_evn_dt}&to={end_evn_dt}&timezone={timezone}&additional_fields={fields}&maximum_rows={maximum_rows}'

print(f'URL = {csv_url}')

# file_output=f'{rec_date}_{app_id}_{report_type}.csv'
FILE_OUTPUT='{}-{}-{}-to-{}.csv'.format(app_id, report_type, start_evn_dt, end_evn_dt)

headers = {
    'content-type': "text/csv; charset=UTF-8"
}

body = json.dumps({
})

# response function
def executeRestApi(verb, url, headers, body):
    res = None
    # Make API request, get response object back, create dataframe from above schema.
    try:
        if verb == "get":
            res = requests.get(url,  data=body, headers=headers)
        elif verb == "post":
            res = requests.post(url, data=body, headers=headers)
        else:
            print("another HTTP verb action")
    except Exception as e:
            return e

    if res != None and res.status_code == 200:
        return res
    return None


res = executeRestApi("get", csv_url, headers, body)

if res.status_code != 200:
  if res.status_code == 404:
    print('There is a problem with the request URL. Make sure that it is correct')
  else:
    print('There was a problem retrieving data: ', res.text)
else:
  f = open(FILE_OUTPUT, 'w', newline='', encoding="utf-8")
  f.write(res.text)
  f.close()

ACCESS_KEY = 'AKIAX74AEFLZWRCUN33C'
SECRET_KEY = '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'


def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    try:
        s3.upload_file(local_file, bucket, s3_file)
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False

uploaded = upload_to_aws(FILE_OUTPUT, BUCKET_NAME, f'{PATH_NAME}/{FILE_OUTPUT}')


job.commit()
