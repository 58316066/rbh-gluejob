import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame


def sparkSqlQuery(glueContext, query, mapping, transformation_ctx) -> DynamicFrame:
    for alias, frame in mapping.items():
        frame.toDF().createOrReplaceTempView(alias)
    result = spark.sql(query)
    return DynamicFrame.fromDF(result, glueContext, transformation_ctx)


args = getResolvedOptions(sys.argv, ["JOB_NAME", "TS_FILTER"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Variables declaration
filter_ts = args["TS_FILTER"]

# Script generated for node Google BigQuery Connector for AWS Glue 3.0
GoogleBigQueryConnectorforAWSGlue30_node1633681551699 = (
    glueContext.create_dynamic_frame.from_options(
        connection_type="marketplace.spark",
        connection_options={
            "parentProject": "robinhood-f74c9",
            "table": "robinhood-f74c9.analytics_230355361.events_20211004",
            "connectionName": "gcp-bq-glue-3-conn",
        },
        transformation_ctx="GoogleBigQueryConnectorforAWSGlue30_node1633681551699",
    )
)

# Script generated for node Spark SQL
SqlQuery0 = f"""select * from myDataSource 
where event_timestamp <= {filter_ts}"""
SparkSQL_node1633685446489 = sparkSqlQuery(
    glueContext,
    query=SqlQuery0,
    mapping={"myDataSource": GoogleBigQueryConnectorforAWSGlue30_node1633681551699},
    transformation_ctx="SparkSQL_node1633685446489",
)

# Script generated for node S3 bucket
S3bucket_node3 = glueContext.write_dynamic_frame.from_options(
    frame=SparkSQL_node1633685446489,
    connection_type="s3",
    format="glueparquet",
    connection_options={
        "path": "s3://nat-fd-bronze/gcp_nonprod/events_intraday/",
        "partitionKeys": ["event_date"],
    },
    format_options={"compression": "snappy"},
    transformation_ctx="S3bucket_node3",
)

job.commit()
