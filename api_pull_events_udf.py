import sys
import requests
import json
import csv
import datetime
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue import DynamicFrame

from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, col, explode, lit
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, ArrayType
from awsglue.dynamicframe import DynamicFrameCollection
from pyspark.sql import Row
# from default_api_pull_event import config



# Setup Params
# GET_AGRS = config.get('GET_AGRS')

# print("start process")
args = getResolvedOptions(sys.argv, ["JOB_NAME", "EVENT_NAME" , "APP_ID", "EVENT_FROM_DT", "EVENT_TO_DT", "MAXIMUM_ROW","FIELDS","API_TOKEN","TIMEZONE","FIELD_PARTITION_S3","S3_OUTPUT_FILE"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Variables declaration
rec_start_date = (datetime.datetime.now() + datetime.timedelta(hours = 7)).strftime('%Y-%m-%d %H:%M:%S')
event = args["EVENT_NAME"]
app_id=args["APP_ID"]
start_evn_dt=args["EVENT_FROM_DT"]
end_evn_dt=args["EVENT_TO_DT"]
maximum_rows=args["MAXIMUM_ROW"]
fields=args["FIELDS"]
api_token=args["API_TOKEN"]
timezone=args["TIMEZONE"]
FIELD_PARTITION_S3=args["FIELD_PARTITION_S3"]
S3_OUTPUT_FILE = args["S3_OUTPUT_FILE"]

csv_url = f'https://hq.appsflyer.com/export/{app_id}/{event}/v5?api_token={api_token}&from={start_evn_dt}&to={end_evn_dt}&timezone={timezone}&additional_fields={fields}&maximum_rows={maximum_rows}'

print(f'URL = {csv_url}')
print(f'EVENT_FROM_DT = {start_evn_dt}')
print(f'EVENT_TO_DT   = {end_evn_dt}')

headers = {
    'content-type': "text/csv; charset=UTF-8"
}

body = json.dumps({
})
# response function - udf
def executeRestApi(verb, url, headers, body):
    res = None
    # Make API request, get response object back, create dataframe from above schema.
    try:
        if verb == "get":
            res = requests.get(url,  data=body, headers=headers)
        elif verb == "post":
            res = requests.post(url, data=body, headers=headers)
        else:
            print("another HTTP verb action")
    except Exception as e:
            return e

    if res != None and res.status_code == 200:
        # return json.loads(res.text)
        return res.text

    return None


schema = StringType()

# udf_executeRestApi = udf(executeRestApi, schema)

res = executeRestApi('get', csv_url, headers, body)
print("::::::::::::::::::::::::::::Response::::::::::::::::::::::::::::")
print(res)
print("::::::::::::::::::::::::::::Response::::::::::::::::::::::::::::")
lines = res.splitlines()
reader = csv.reader(lines)
parsed_csv = list(reader)
print('len(parsed_csv) : ' + str(len(parsed_csv)))
if len(parsed_csv) > 1 :
    header = parsed_csv[0]
    header = [s.strip().lower().replace(' ','_') for s in header]
    data=parsed_csv[1::]
    
    eventDF = spark.createDataFrame(data=data, schema = header)\
                .withColumn('report_type', lit(event))\
                .withColumn('event_dt_year', col("event_time").substr(1, 4))\
                .withColumn('event_dt_month', col("event_time").substr(6, 2))\
                .withColumn('event_dt_date', col("event_time").substr(9, 2))\
                .withColumn('rec_start_date', lit(datetime.datetime.now().strftime('%Y-%m-%d')))
    eventDF.printSchema()
    eventDF.limit(5).show()
    
    
    glue_df = DynamicFrame.fromDF(eventDF, glueContext, "UDF REST Demo")
    dynamic_frame = DynamicFrameCollection({"CustomTransform0": glue_df}, glueContext)
    
    
    # Script generated for node S3 bucket
    S3bucket_node3 = glueContext.write_dynamic_frame.from_options(
        frame=dynamic_frame,
        connection_type="s3",
        format="glueparquet",
        connection_options={
            "path": S3_OUTPUT_FILE,
            "partitionKeys": FIELD_PARTITION_S3.replace(" ","").split(','),
        },
        format_options={"compression": "snappy"},
        transformation_ctx="S3bucket_node3",
    )
else:
    print("No Data...")


job.commit()
