default_config = {
    "GET_AGRS" : ["JOB_NAME", "EVENT_NAME" , "APP_ID", "EVENT_FROM_DT", "EVENT_TO_DT", "MAXIMUM_ROW","FIELDS","API_TOKEN","TIMEZONE","FIELD_PARTITION_S3","S3_OUTPUT_FILE"]
} 

    #"FIELD_PARTITION_S3" : ["rec_dt_year","rec_dt_month","rec_dt_date"]
    # "GLUEJOB_NAME" : "job_api_pull_appsflyer",
    # "ARG_EVENT_NAME" : 'organic_installs_report',
    # "ARG_APP_ID" : 'id111180000',
    # "ARG_EVENT_FROM_DT" : '2021-10-10',
    # "ARG_EVENT_TO_DT" : '2021-10-15',
    # "ARG_API_TOKEN" : '23f6fcfd-5ea2-420f-b2a2-8fd9f54dc789',
    # "ARG_TIMEZONE" : "Asia%2fBangkok",
    # "ARG_MAXIMUM_ROW" : "1000000",
    # "ARG_FIELDS" : "device_model,keyword_id,store_reinstall,deeplink_url,oaid,install_app_store,gp_referrer,gp_click_time,gp_install_begin,amazon_aid,keyword_match_type,att,conversion_type,campaign_type,is_lat",
    # "GLUE_SCRIPT_LOCN" : 's3://nat-glue-assets/api_pull_events.py',
    # "S3_BUCKET" : 's3://nat-mwaa-de-airflow-poc',
    # "GLUEJOB_DESC" : "pull csv file from appsflyer using GET method dag pull_requert_daily",
    # "IAM_ROLE_NAME" : 'poc-glue-bq-connector',
    # "REGION_NAME" : 'ap-southeast-1',
    # "NUM_OF_DPUS" : 0,
    # "DAG_TAC" : ['poc'],
    # "RETRY_LIMIT" : 0,
    # "NUM_OF_WORKERS" : 2,
    # "GlueVersion": "3.0",
    # "WorkerType": "G.1X"